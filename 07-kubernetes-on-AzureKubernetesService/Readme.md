# Deploy on Deploy an Azure Kubernetes Service (AKS)
---

In this quickstart, you deploy an Azure Kubernetes Service (AKS) cluster using the Azure CLI. AKS is a managed Kubernetes service that lets you quickly deploy and manage clusters. A multi-container application that includes a web front end and a Redis instance is run in the cluster. You then see how to monitor the health of the cluster and pods that run your application.

Voting app deployed in Azure Kubernetes Service
![catture](voting-app-deployed-in-azure-kubernetes-service.png)

This quickstart assumes a basic understanding of Kubernetes concepts.

- [Create container images from an application](https://docs.microsoft.com/en-us/azure/aks/tutorial-kubernetes-prepare-app)
- [Upload container images to the Azure Container Registry](https://docs.microsoft.com/en-us/azure/aks/tutorial-kubernetes-prepare-acr)
- [Deploy an AKS cluster](https://docs.microsoft.com/en-us/azure/aks/tutorial-kubernetes-deploy-cluster)
- [Run your container images in Kubernetes](https://docs.microsoft.com/en-us/azure/aks/tutorial-kubernetes-deploy-application)
- [Scale an application and Kubernetes infrastructure](https://docs.microsoft.com/en-us/azure/aks/tutorial-kubernetes-scale)
- [Update an application running in Kubernetes](https://docs.microsoft.com/en-us/azure/aks/tutorial-kubernetes-app-update)
- [Upgrade AKS cluster](https://docs.microsoft.com/en-us/azure/aks/tutorial-kubernetes-upgrade-cluster)

To get the instructions follow the hyperlinks.