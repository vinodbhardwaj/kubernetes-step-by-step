kubectl version --client
kubectl version --client --short
kubectl version --short

minikube start --hyperv

minikube status

minikube stop

minikube delete   #  To clear minikube's local state

kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta8/aio/deploy/recommended.yaml

https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended.yaml

minikube dashboard