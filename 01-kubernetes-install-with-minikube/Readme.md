# Installing Kubernernetes with Minikube
---
Kubernetes is a portable, extensible, open-source platform for managing containerized workloads and services, that facilitates both declarative configuration and automation. It has a large, rapidly growing ecosystem. Kubernetes services, support, and tools are widely available.

The name Kubernetes originates from Greek, meaning helmsman or pilot. Google open-sourced the Kubernetes project in 2014. Kubernetes builds upon a decade and a half of experience that Google has with running production workloads at scale, combined with best-of-breed ideas and practices from the community.

The architecture of Kubernetes, which is a client-server type, can be represented simply, as shown in the
following diagram.
![architecture](images/Kubernetes-architecture.png)
*Kubernetes architecture illustrating Container Runtime as Docker. Kubernetes currently support Docker, containerd, and CRI-O as its container runtime. [Image source](https://blog.sensu.io/how-kubernetes-works)*

## Installing Minikube
Minikube is a tool that makes it easy to run Kubernetes locally. Minikube runs a single-node Kubernetes cluster inside a Virtual Machine (VM) on your laptop for users looking to try out Kubernetes or develop with it day-to-day.
The installation instructions for each operation system is provided [here](https://kubernetes.io/docs/tasks/tools/install-minikube/).

### _Install kubectl_ 
Kubectl is a command line tool for controlling Kubernetes clusters. kubectl looks for a file named config in the `$HOME/.kube` directory. You can install kubectl according to the instructions in [Install and Set Up kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/).
###
On windows, Kubectl can be installed in several ways
* Manual installation
  1. Download the latest release v1.17.0 from [this link](https://storage.googleapis.com/kubernetes-release/release/v1.17.0/bin/windows/amd64/kubectl.exe).

     Or if you have curl installed, use this command:

     curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.17.0/bin/windows/amd64/kubectl.exe
     To find out the latest stable version (for example, for scripting), take a look at https://storage.googleapis.com/kubernetes-release/release/stable.txt.

  2. Add the binary in to your PATH.

  3. Test to ensure the version of kubectl is the same as downloaded:

        ```shell
        kubectl version --client
        ```
* Install on Windows using Chocolatey 
  [Chocolatey](https://chocolatey.org/) package manager should be installed. 
  ```shell
   choco install kubernetes-cli
   ```
### _Install Minikube_ 
If you do not already have a hypervisor installed, install one of these: [Hyper-V](https://msdn.microsoft.com/en-us/virtualization/hyperv_on_windows/quick_start/walkthrough_install) or [VirtualBox](https://www.virtualbox.org/wiki/Downloads).

* **Install Minikube using an installer executable**
To install Minikube manually on Windows using [Windows Installer](https://docs.microsoft.com/en-us/windows/desktop/msi/windows-installer-portal), download [`minikube-installer.exe`](https://github.com/kubernetes/minikube/releases/latest/download/minikube-installer.exe) and execute the installer.


* **Install Minikube using Chocolatey**
The easiest way to install Minikube on Windows is using [Chocolatey](https://chocolatey.org/) (run as an administrator):

    ```shell
    choco install minikube
    ```
  After Minikube has finished installing, close the current CLI session and restart. Minikube should have been added to your path automaticall

### _Confirm Installation_ 
To confirm successful installation of both a hypervisor and Minikube, you can run the following command to start up a local Kubernetes cluster:
```shell
minikube start --vm-driver=<driver_name>
<driver_name> can be hyperv, virtualbox, kvm2, vmware, ... 
```
Once minikube start finishes, run the command below to check the status of the cluster:

```shell
minikube status
```
If your cluster is running, the output from minikube status should be similar to:

```shell
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```
After you have confirmed whether Minikube is working with your chosen hypervisor, you can continue to use Minikube or you can stop your cluster. To stop your cluster, run:

```shell
minikube stop
```
If you you need to clear minikube's local state:

```shell
minikube delete
```
## Installing Kubernetes Dashoard

Kubernetes dashboard is a web-based Kubernetes user interface. You can use Dashboard to deploy containerized applications to a Kubernetes cluster, troubleshoot your containerized application, and manage the cluster resources. You can use Dashboard to get an overview of applications running on your cluster, as well as for creating or modifying individual Kubernetes resources (such as Deployments, Jobs, DaemonSets, etc). Kubernetes dashboard, which is a pre-packaged containerized web
application that will be deployed in our cluster, we will run the following command in a
Terminal:
```shell
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta8/aio/deploy/recommended.yaml
```

You can access the dashboard by running the following command

```shell
minikube dashboard
```