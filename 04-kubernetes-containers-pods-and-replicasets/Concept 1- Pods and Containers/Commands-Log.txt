
cat my-first-pod.yaml
======================
apiVersion: v1
kind: Pod
metadata:
  name: my-first-pod
spec:
  containers:
  - name: my-nginx
    image: nginx
  - name: my-centos
    image: centos
    command: ["/bin/sh", "-c", "while : ;do curl http://localhost:80/; sleep 10; done"]


kubectl create -f my-first-pod.yaml

$ kubectl get pods

kubectl exec my-first-pod -it -c my-centos -- /bin/bash

curl -L http://localhost:80

kubectl get pods -o wide


cat my-second-pod.yaml
======================
apiVersion: v1
kind: Pod
metadata:
  name: my-second-pod
spec:
  containers:
  - name: my-nginx
    image: nginx
  - name: my-centos
    image: centos
    command: ["/bin/sh", "-c", "while : ;do curl http://localhost:80/; sleep 10; done"]

kubectl create -f my-second-pod.yaml

kubectl get pods


Delete your Pod from the

kubectl delete pods --all
kubectl get pods
