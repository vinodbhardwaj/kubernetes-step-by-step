# Deploy and Scale a Spring boot Microservice on Kubernetes
---

As part of this lab, we'll:

- Install Minikube on our local machine
- Develop an example application consisting of two Spring Boot services
- Set up the application on a one-node cluster using Minikube
- Deploy the application using config files

## 1. Starting Minikube
First, check if minikube is started stated using this command
```shell
$ minikube status
```
If it is not started, start it using the following command:
```shell
$ minikube start
```
After that, we can verify that kubectl communicates correctly with our cluster:

```shell
$ minikube status
```
```
$> kubectl cluster-info
```
To further debug and diagnose cluster problems, use '`kubectl cluster-info dump`'.

Finally, we can inspect the state of our cluster:

```shell
$ minikube dashboard
```
This command opens a site in our default browser, which provides an extensive overview about the state of our cluster.

## 2. Demo Application
As our cluster is now running and ready for deployment, we need a demo application.

For this purpose, we'll create a simple "Hello world" application, consisting of two Spring Boot services, which we'll call **frontend** and **backend**. The source code is given this forlder. 

The backend provides one REST endpoint on port **8080**, returning a String containing its hostname. The frontend is available on port **8081**, it will simply call the backend endpoint and return its response.

Import both of the services under Eclipse STS and check that they work correctly. The backend should be started before the frontened.

## 3. Create and Push Docker images to DockerHub

Build and push the backend image using these commands
```shell
$ docker build  --tag=mromdhani/demo-backend:latest  
$ docker push mromdhani/demo-backend:latest
```

Build and push the frontend  image using these commands
```shell
$ docker build  --tag=mromdhani/demo-frontend:latest  
$ docker push mromdhani/demo-frontend:latest
```
Simple Deployment Using Imperative Commands
In a first step, we'll create a Deployment for our demo-backend app, consisting of only one Pod. Based on that, we'll discuss some commands so we can verify the Deployment, inspect logs, and clean it up at the end.

## 4. Creating the Deployment
We'll use kubectl, passing all required commands as arguments:

For the backend:

```shell 
kubectl create deployment demo-backtend --image=mromdhani/demo-backend:latest
```

For the backend:

```shell 
kubectl create deployment demo-frontend --image=mromdhani/demo-frontend:latest
```

Verifying the Deployment
Now, we can check whether the deployment was successful:

```shell
kubectl get deployments
```
The output looks like this:

```shell
$> kubectl get pods
$> kubectl logs <pod id>
```
## 5. Creating a Service for the Deployment
To make the REST endpoint of our backend app available, we need to create a Service:
For the backend
```shell
kubectl expose deployment demo-backend --type=LoadBalancer --port=8080  --name=demo-backend 
```
For the frontend
```shell
kubectl expose deployment demo-frontend --type=LoadBalancer --port=8081  --name=demo-frontend 
```
Check that the services has been exposed correctly.
```shell
kubectl get services
```
You call the serice frontend
```shell
minikube service demo-frontend
```

## 6. Deployment Using Configuration Files
For more complex setups, configuration files are a better choice, instead of passing all parameters via command line arguments.

Configurations files are a great way of documenting our deployment, and they can be versioned controlled.

#### Deployment Definition for Backend App

We can define the Deployment as follows. Create a file named `end-deployment.yaml`:
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: demo-backend
spec:
  selector:
      matchLabels:
        app: demo-backend
  replicas: 3
  template:
    metadata:
      labels:
        app: demo-backend
    spec:
      containers:
        - name: demo-backend
          image: mromdhani/demo-backend:latest
          ports:
            - containerPort: 8080
```
We create a Deployment named demo-backend, indicated by the metadata: name field.

- _The spec_: selector field defines how the Deployment finds which Pods to manage. In this case, we merely select on one label defined in the Pod template (app: demo-backend).

- We want to have three replicated Pods, which we indicate by the replicas field.

- The _template_ field defines the actual Pod:

- The Pods are _labeled_ as app: demo-backend
- The template: _spec_ field indicates that each Pod replication runs one container, demo-backend, with version latest
- The Pods open port 8080

#### Service Definition for Backend App
We can define the  Service as follows. This definition can be appended to the deployment file.
```yaml
kind: Service
apiVersion: v1
metadata:
  name: demo-backend
spec:
  selector:
    app: demo-backend
  ports:
  - protocol: TCP
    port: 8080
  type: ClusterIP
```
We create a Service named demo-backend, indicated by the metadata: name field.
It targets TCP port 8080 on any Pod with the app=demo-backend label.

Finally, type: _ClusterIP_ indicates that it is only available from inside of the cluster (as we want to call the endpoint from our demo-frontend app this time, _but not directly from a browser anymore_, as in the previous example).


We can now trigger the deployment:

```
kubectl create -f backend-deployment.yaml
```
Let's verify that the deployment was successful:

```shell
kubectl get deployments
```
```shell
kubectl get services
```

## Deployment and service definition for the frontend service

Both frontend and backend are almost identical, the only difference between backend and frontend is the spec of the Service: For the frontend, we define the type as LoadBalancer (as we want to make the frontend available to outside of the cluster). The backend only has to be reachable from within the cluster, therefore, the type was ClusterIP.
We can now trigger the deployment:

```
kubectl create -f front-deployment.yaml
```
Let's verify that the deployment of the frontend service was successful:

```shell
kubectl get deployments
```
```shell
kubectl get services
```

## Testing the frontend Service

We can call the REST endpoint of the frontend application as follows:

```shell
 minikube service demo-frontend
```
## 7. Cleaning up Services and Deployments
In the end, we can clean up by removing Services and Deployments:

```shell
kubectl delete service demo-frontend
kubectl delete deployment demo-frontend
kubectl delete service demo-backend
kubectl delete deployment demo-backend
```