# Kubernetes Basic Concepts (Interactive tutorials)
---
This tutorial provides a walkthrough of the basics of the Kubernetes cluster orchestration system. Each module contains some background information on major Kubernetes features and concepts, and includes an interactive online tutorial. These interactive tutorials let you manage a simple cluster and its containerized applications for yourself.

Using the interactive tutorials, you can learn to:

- Deploy a containerized application on a cluster.
- Scale the deployment.
- Update the containerized application with a new software version.
- Debug the containerized application.

The tutorials use `Katacoda` to run a virtual terminal in your web browser that runs Minikube, a small-scale local deployment of Kubernetes that can run anywhere. There's no need to install any software or configure anything; each interactive tutorial runs directly out of your web browser itself.


_**What can Kubernetes do for you?**_
With modern web services, users expect applications to be available 24/7, and developers expect to deploy new versions of those applications several times a day. Containerization helps package software to serve these goals, enabling applications to be released and updated in an easy and fast way without downtime. Kubernetes helps you make sure those containerized applications run where and when you want, and helps them find the resources and tools they need to work. Kubernetes is a production-ready, open source platform designed with Google's accumulated experience in container orchestration, combined with best-of-breed ideas from the community.

## Kubernetes Basics modules

![modules](images/BasicModules.png)

### 1. Create a Kubernetes Cluster
_Objectives_
- Learn what a Kubernetes cluster is.
- Learn what Minikube is.
- Start a Kubernetes cluster using an online terminal.
 ![modules](images/module_01_cluster.svg) 
[Go to the Interactive Tutorial](https://kubernetes.io/docs/tutorials/kubernetes-basics/create-cluster/cluster-intro/).

### 2. Explore your applications
_Objectives_
- Learn about application Deployments.
- Deploy your first app on Kubernetes with kubectl.
 ![modules](images/module_02_first_app.svg) 
[Go to the Interactive Tutorial](https://kubernetes.io/docs/tutorials/kubernetes-basics/deploy-app/deploy-intro/).

### 3. Deply an application
_Objectives_
- Learn about Kubernetes Pods.
- Learn about Kubernetes Nodes.
Troubleshoot deployed applications.
 ![modules](images/module_03_nodes.svg) 
[Go to the Interactive Tutorial](https://kubernetes.io/docs/tutorials/kubernetes-basics/deploy-app/deploy-intro/).

### 4. Expose your application publicly
_Objectives_
- Learn about a Service in Kubernetes
- Understand how labels and LabelSelector objects relate to a Service
- Expose an application outside a Kubernetes cluster using a Service

 ![modules](images/module_04_services.svg) 
[Go to the Interactive Tutorial](https://kubernetes.io/docs/tutorials/kubernetes-basics/expose/expose-intro/).

### 5. Scale your application
_Objectives_
- Scale an app using kubectl.

 ![modules](images/module_05_scaling2.svg) 
[Go to the Interactive Tutorial](https://kubernetes.io/docs/tutorials/kubernetes-basics/scale/scale-intro/).

### 6. Scale your application
_Objectives_
- Perform a rolling update using kubectl.

 ![modules](images/module_06_rollingupdates4.svg) 
[Go to the Interactive Tutorial](https://kubernetes.io/docs/tutorials/kubernetes-basics/update/update-intro/).