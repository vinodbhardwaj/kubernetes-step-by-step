Deploying to Azure Kubernetes Services (AKS) Using GitLabCI
---

Gitlab allows you to provide details of your existing Kubernetes cluster. In return, it offers an ever-increasing series of benefits for doing this, as part of Gitlab’s drive to become your main hub for every stage from “idea to production”.

Benefits of enabling this integration include:

- Links to logs, monitoring, terminals and more in Gitlab’s UI. Nice.
- “Auto dev ops” which is Gitlab’s attempt at a zero-configuration CI process. I suspect that at the moment if you’re hardcore enough to want to use Kubernetes, you’re probably going to want to customise your CI process by declaring your own .gitlab-ci.yml file. So this benefit seems not so interesting to me.
- Details of your cluster are exposed as environment variables in the CI. Could be useful if you’re using Kubectl directly, but irrelevant if you’re instead using your cloud provider’s cli tool for authentication? I should check that out more.
- A UI for automatically provisioning your cluster with useful tools like Helm (for deployments) and Prometheus (for monitoring). Gitlab isn’t doing anything fancy here, but I’d rather be under their umbrella than banging on the cli on my own so I like the idea of this. 

### Connecting Gitlab to AKS

Follow this [tutorial](https://gitlab.com/citihub/aks-accelerator/blob/master/docs/06_connecting_gitlab_to_aks.md) and this [StackOverflow](https://gitlab.com/your-account/your-repo/clusters/new).

### Deploying to AKS

Follow these steps by the Accelerator project.
[07 - Using Gitlab CI/CD to build an Image](https://gitlab.com/citihub/aks-accelerator/blob/master/docs/07_gitlab_ci_build_image.md)
[08 - Using Gitlab CI/CD to deploy an Image](https://gitlab.com/citihub/aks-accelerator/blob/master/docs/08_gitlab_ci_deploy_image.md)


More detailed step by step are given in [Accelerator project](https://gitlab.com/citihub/aks-accelerator). This 15 minutes [Youtube video](https://www.youtube.com/watch?v=_H7qYkAt8Bo) illustrates those steps.


