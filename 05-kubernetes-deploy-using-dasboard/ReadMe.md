# Deploying applications using Kubernetes GUI/Dashboard


In this lab we will walk through how to create deploy a new application on Kubernetes cluster.  We could deploy a containerized applications on top of Kubernetes cluster using the various method.  In this example, we will use a method called “deployment” to deploy the application on Kubernetes. Let’s start creating Kubernetes deployment configuration for the new application.

In this example, We will demonstrate how to deploy an nginx application container using Kubenetes GUI/Dashboard.

## Steps

- MiniKube version

    ```shell
    minikube version
    ```

- Ensure “minikube” is up and running fine.
    ```shell
    minikube status
    ```
- Start the dashboard

    ```shell
    minikube dashboard
    ```
- Here is the Kubernetes Dashboard.
    ![caputure](images/dashboard.png)

- Navigate to workloads – > Deployments tab. Click on “CREATE”
    ![capture](images/Kubernetes-Deployments-Tab.png)

- Copy the following content in the input tab. Kubernetes Deployment supports YAML format.
    ```yaml
    apiVersion: apps/v1 # for versions before 1.9.0 use apps/v1beta2
    kind: Deployment
    metadata:
    name: nginx-deployment
    spec:
    selector:
        matchLabels:
        app: nginx
    replicas: 2 # tells deployment to run 2 pods matching the template
    template:
        metadata:
        labels:
            app: nginx
        spec:
        containers:
        - name: nginx
            image: nginx:1.7.9
            ports:
            - containerPort: 80
    ```
Here is the snapshot of text input. Click on upload to deploy nginx application service in Kubernetes cluster.

    ![capture](images/ngnix-Kubernetes-Deployment-file.png)

- Kubernetes has started the pod deployment for the nginx container.

    ![capture](images/ngnix-Deployment-in-Kubernetes-Cluster.png)
- Once the deployment is successful, you can see the workload state like below.
    ![capture](images/ngnix-Minikube-Kubernetes-Deployment.png)

- Expose the “nginx” application container to access.

    ```shell
    kubectl expose deployment/nginx-deployment --type="NodePort" --port 80
    ```
- Validate the service of ngnix.

    ```
    kubectl get services nginx-deployment
    ```
- Execute the “minikube service” command to access the “nginx” application service.
    ```shell
    minikube service nginx-deployment
    * Opening kubernetes service default/nginx-deployment in default browser...
    START /usr/bin/firefox "http://192.168.39.109:32339"
    Running without a11y support!
    ```
    ![capture](images/nginx.png)